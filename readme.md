Listens for HTTP posts on specified endpoint and forwards received messages to a Telegram user through a bot.

### Installation

1. Copy config.json_ to config.json
2. In the config:
   1. Fill `token` with the Telegram bot token (obtained from botfather)
   2. Fill `tgUserID` with the Telegram ID of the user whom you want to forward messages
   3. Fill `endpoint` with the path to listen on (defaults to /posttotg)
3. Install dependencies with `npm install`
4. Run the bot with `npm start` (by default it will listen on port 3000 - you can override it with the PORT env variable)
5. Send HTTP post requests to the address of the bot and see them in your Telegram

### Configuring Nginx proxy pass for specific route to post to posttotg from your site

```
upstream tgpost {
    server 127.0.0.1:3005;
    keepalive 32;
}

server {
    root /var/www/html;
    server_name xxx.com;

    location /posttotg {
                 proxy_pass http://tgpost;

                 proxy_http_version 1.1;
                 proxy_set_header Upgrade $http_upgrade;
                 proxy_set_header Connection "upgrade";
                 proxy_set_header Host $http_host;
                 proxy_set_header X-Real-IP $remote_addr;
                 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                 proxy_set_header X-Forward-Proto http;
                 proxy_set_header X-Nginx-Proxy true;

                 proxy_temp_file_write_size 64k;
                 proxy_connect_timeout 10080s;
                 proxy_send_timeout 10080;
                 proxy_read_timeout 10080;
                 proxy_buffer_size 64k;
                 proxy_buffers 16 32k;
                 proxy_busy_buffers_size 64k;
                 proxy_redirect off;
                 proxy_request_buffering off;
                 proxy_buffering off;

                 access_log /var/www/logs/posttotg.access.log;
                 error_log /var/www/logs/posttotg.error.log;
        }

}
```