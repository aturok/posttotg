var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const TelegramBot = require('node-telegram-bot-api');

const config = require('./config.json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function jsonResponse(response, data) {
    response.setHeader('Content-Type', 'application/json');
    return response.send(JSON.stringify(data));
}

function sendToTg(data) {
    let bot = new TelegramBot(config.token, {polling: {autoStart: false}});
    bot.sendMessage(config.tgUserID, JSON.stringify(data));
}

app.post(config.endpoint,
         function(req, res){
             sendToTg(req.body);
             return jsonResponse(res, {result: 'ok'});
         });

app.listen(process.env.PORT || 3000);
